package com.sda.finalproject.service;

import com.sda.finalproject.model.Item;
import com.sda.finalproject.model.ItemBody;
import com.sda.finalproject.model.User;
import com.sda.finalproject.model.UserBody;

import java.util.List;

public interface ItemService {
    Item save(ItemBody item);

    List<Item> findAll();

    void delete(int id);

    Item findOne(String title);

    Item findById(int id);

    ItemBody update(ItemBody itemBody);
}
