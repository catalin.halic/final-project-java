package com.sda.finalproject.service;

import com.sda.finalproject.model.User;
import com.sda.finalproject.model.UserBody;

import java.util.List;

public interface UserService {

    User save(UserBody user);

    List<User> findAll();

    void delete(int id);

    User findOne(String username);

    User findById(int id);

    UserBody update(UserBody userBody);
}
